import ~/.config/nixpkgs/devshell.nix {
  cfg = {
    go = true;
    nix = true;
    yarn = false;
    json = true;
    caddy = false;
    google = true;
    shell = true;
    elm = false;
    web = false;
  };
}
