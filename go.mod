module bitbucket.org/vspry/terraform-provider-restapi

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/hashicorp/terraform-plugin-sdk/v2 v2.10.0
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0
)
