package restapi

import (
	"context"
	"fmt"
	"math"

	"github.com/hashicorp/terraform-plugin-sdk/v2/diag"
	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
)

/*Provider implements the REST API provider*/
func Provider() *schema.Provider {
	return &schema.Provider{
		Schema: map[string]*schema.Schema{
			"uri": {
				Type:        schema.TypeString,
				Required:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_URI", nil),
				Description: "URI of the REST API endpoint. This serves as the base of all requests.",
			},
			"insecure": {
				Type:        schema.TypeBool,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_INSECURE", nil),
				Description: "When using https, this disables TLS verification of the host.",
			},
			"username": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_USERNAME", nil),
				Description: "When set, will use this username for BASIC auth to the API.",
			},
			"password": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_PASSWORD", nil),
				Description: "When set, will use this password for BASIC auth to the API.",
			},
			"headers": {
				Type:        schema.TypeMap,
				Elem:        schema.TypeString,
				Optional:    true,
				Description: "A map of header names and values to set on all outbound requests. This is useful if you want to use a script via the 'external' provider or provide a pre-approved token or change Content-Type from `application/json`. If `username` and `password` are set and Authorization is one of the headers defined here, the BASIC auth credentials take precedence.",
			},
			"use_cookies": {
				Type:        schema.TypeBool,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_USE_COOKIES", nil),
				Description: "Enable cookie jar to persist session.",
			},
			"timeout": {
				Type:        schema.TypeInt,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_TIMEOUT", 0),
				Description: "When set, will cause requests taking longer than this time (in seconds) to be aborted.",
			},
			"id_attribute": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_ID_ATTRIBUTE", nil),
				Description: "When set, this key will be used to operate on REST objects. For example, if the ID is set to 'name', changes to the API object will be to http://foo.com/bar/VALUE_OF_NAME. This value may also be a '/'-delimeted path to the id attribute if it is multple levels deep in the data (such as `attributes/id` in the case of an object `{ \"attributes\": { \"id\": 1234 }, \"config\": { \"name\": \"foo\", \"something\": \"bar\"}}`",
			},
			"create_method": {
				Type:        schema.TypeString,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_CREATE_METHOD", nil),
				Description: "Defaults to `POST`. The HTTP method used to CREATE objects of this type on the API server.",
				Optional:    true,
			},
			"read_method": {
				Type:        schema.TypeString,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_READ_METHOD", nil),
				Description: "Defaults to `GET`. The HTTP method used to READ objects of this type on the API server.",
				Optional:    true,
			},
			"update_method": {
				Type:        schema.TypeString,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_UPDATE_METHOD", nil),
				Description: "Defaults to `PUT`. The HTTP method used to UPDATE objects of this type on the API server.",
				Optional:    true,
			},
			"destroy_method": {
				Type:        schema.TypeString,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_DESTROY_METHOD", nil),
				Description: "Defaults to `DELETE`. The HTTP method used to DELETE objects of this type on the API server.",
				Optional:    true,
			},
			"copy_keys": {
				Type: schema.TypeList,
				Elem: &schema.Schema{
					Type: schema.TypeString,
				},
				Optional:    true,
				Description: "When set, any PUT to the API for an object will copy these keys from the data the provider has gathered about the object. This is useful if internal API information must also be provided with updates, such as the revision of the object.",
			},
			"write_returns_object": {
				Type:        schema.TypeBool,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_WRO", nil),
				Description: "Set this when the API returns the object created on all write operations (POST, PUT). This is used by the provider to refresh internal data structures.",
			},
			"create_returns_object": {
				Type:        schema.TypeBool,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_CRO", nil),
				Description: "Set this when the API returns the object created only on creation operations (POST). This is used by the provider to refresh internal data structures.",
			},
			"xssi_prefix": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_XSSI_PREFIX", nil),
				Description: "Trim the xssi prefix from response string, if present, before parsing.",
			},
			"rate_limit": {
				Type:        schema.TypeFloat,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_RATE_LIMIT", math.MaxFloat64),
				Description: "Set this to limit the number of requests per second made to the API.",
			},
			"test_path": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_TEST_PATH", nil),
				Description: "If set, the provider will issue a read_method request to this path after instantiation requiring a 200 OK response before proceeding. This is useful if your API provides a no-op endpoint that can signal if this provider is configured correctly. Response data will be ignored.",
			},
			"debug": {
				Type:        schema.TypeBool,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_DEBUG", nil),
				Description: "Enabling this will cause lots of debug information to be printed to STDOUT by the API client.",
			},
			"oauth_client_id": {
				Type:        schema.TypeString,
				Description: "client id",
				Optional:    true,
			},
			"oauth_client_secret": {
				Type:        schema.TypeString,
				Description: "client secret",
				Optional:    true,
			},
			"oauth_token_endpoint": {
				Type:        schema.TypeString,
				Description: "oauth token endpoint",
				Optional:    true,
			},
			"oauth_scopes": {
				Type:        schema.TypeList,
				Elem:        &schema.Schema{Type: schema.TypeString},
				Optional:    true,
				Description: "oauth scopes",
			},
			"cert_file": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_CERT_FILE", nil),
				Description: "When set with the key_file parameter, the provider will load a client certificate for mTLS authentication.",
			},
			"key_file": {
				Type:        schema.TypeString,
				Optional:    true,
				DefaultFunc: schema.EnvDefaultFunc("REST_API_KEY_FILE", nil),
				Description: "When set with the cert_file parameter, the provider will load a client certificate for mTLS authentication. Note that this mechanism simply delegates to golang's tls.LoadX509KeyPair which does not support passphrase protected private keys. The most robust security protections available to the key_file are simple file system permissions.",
			},
		},
		ResourcesMap: map[string]*schema.Resource{
			"restapi_object": resourceRestAPI(),
		},
		DataSourcesMap: map[string]*schema.Resource{
			"restapi_object": dataSourceRestAPI(),
		},
		ConfigureContextFunc: providerConfigure,
	}
}

func providerConfigure(ctx context.Context, d *schema.ResourceData) (interface{}, diag.Diagnostics) {
	var diags diag.Diagnostics

	/* As "data-safe" as terraform says it is, you'd think
	   it would have already coaxed this to a slice FOR me */
	copyKeys := make([]string, 0)
	if iCopyKeys := d.Get("copy_keys"); iCopyKeys != nil {
		for _, v := range iCopyKeys.([]interface{}) {
			copyKeys = append(copyKeys, v.(string))
		}
	}

	headers := make(map[string]string)
	if iHeaders := d.Get("headers"); iHeaders != nil {
		for k, v := range iHeaders.(map[string]interface{}) {
			headers[k] = v.(string)
		}
	}

	opt := &apiClientOpt{
		uri:                 d.Get("uri").(string),
		insecure:            d.Get("insecure").(bool),
		username:            d.Get("username").(string),
		password:            d.Get("password").(string),
		headers:             headers,
		useCookies:          d.Get("use_cookies").(bool),
		timeout:             d.Get("timeout").(int),
		idAttribute:         d.Get("id_attribute").(string),
		copyKeys:            copyKeys,
		writeReturnsObject:  d.Get("write_returns_object").(bool),
		createReturnsObject: d.Get("create_returns_object").(bool),
		xssiPrefix:          d.Get("xssi_prefix").(string),
		rateLimit:           d.Get("rate_limit").(float64),
		debug:               d.Get("debug").(bool),
	}

	if v, ok := d.GetOk("create_method"); ok {
		opt.createMethod = v.(string)
	}
	if v, ok := d.GetOk("read_method"); ok {
		opt.readMethod = v.(string)
	}
	if v, ok := d.GetOk("update_method"); ok {
		opt.updateMethod = v.(string)
	}
	if v, ok := d.GetOk("destroy_method"); ok {
		opt.destroyMethod = v.(string)
	}
	if v, ok := d.GetOk("oauth_client_id"); ok {
		opt.oauthClientID = v.(string)
	}
	if v, ok := d.GetOk("oauth_client_secret"); ok {
		opt.oauthClientSecret = v.(string)
	}
	if v, ok := d.GetOk("oauth_token_endpoint"); ok {
		opt.oauthTokenURL = v.(string)
	}
	if v, ok := d.GetOk("oauth_scopes"); ok {
		scopes := v.([]interface{})
		if len(scopes) > 0 {
			opt.oauthScopes = make([]string, len(scopes))
		}
		for i, scope := range scopes {
			opt.oauthScopes[i] = scope.(string)
		}
	}
	if v, ok := d.GetOk("cert_file"); ok {
		opt.certFile = v.(string)
	}
	if v, ok := d.GetOk("key_file"); ok {
		opt.keyFile = v.(string)
	}

	client, err := NewAPIClient(opt)
	if err != nil {
		diags = append(diags, diag.Diagnostic{
			Severity: diag.Error,
			Summary:  fmt.Sprintf("could not create client %v", opt.uri),
			Detail:   fmt.Sprintf("could not create client %v: %v", opt.uri, err),
		})
		return nil, diags
	}

	if v, ok := d.GetOk("test_path"); ok {
		testPath := v.(string)
		_, err := client.sendRequest(client.readMethod, testPath, "")
		if err != nil {
			diags = append(diags, diag.Diagnostic{
				Severity: diag.Error,
				Summary:  fmt.Sprintf("a test request to %v failed", testPath),
				Detail:   fmt.Sprintf("a test request to %v after setting up the provider did not return an OK response - is your configuration correct? %v", testPath, err),
			})
			return nil, diags
		}
	}
	return client, diags
}
