package restapi

import (
	"context"
	"testing"

	"bitbucket.org/vspry/terraform-provider-restapi/fakeserver"

	"github.com/hashicorp/terraform-plugin-sdk/v2/helper/schema"
	"github.com/hashicorp/terraform-plugin-sdk/v2/terraform"
)

var testAccProviders map[string]*schema.Provider
var testAccProvider *schema.Provider

func init() {
	testAccProvider = Provider()
	testAccProviders = map[string]*schema.Provider{
		"restapi": testAccProvider,
	}
}

func TestProvider(t *testing.T) {
	if err := Provider().InternalValidate(); err != nil {
		t.Fatalf("err: %s", err)
	}
}

func TestProvider_impl(t *testing.T) {
	var _ *schema.Provider = Provider()
}

func TestResourceProvider_RequireBasic(t *testing.T) {
	rp := Provider()

	raw := map[string]interface{}{
		"uri": "test",
	}

	rawConfig := terraform.NewResourceConfigRaw(raw)

	d := rp.Configure(context.Background(), rawConfig)
	if d.HasError() {
		t.Fatal(d)
	}
}

func TestResourceProvider_Oauth(t *testing.T) {
	rp := Provider()

	s := []string{"api"}
	scopes := make([]interface{}, len(s))
	for i, v := range s {
		scopes[i] = v
	}

	raw := map[string]interface{}{
		"uri":                  "test",
		"oauth_client_id":      "test",
		"oauth_client_secret":  "test",
		"oauth_token_endpoint": "test",
		"oauth_scopes":         scopes,
	}

	rawConfig := terraform.NewResourceConfigRaw(raw)

	d := rp.Configure(context.Background(), rawConfig)
	if d.HasError() {
		t.Fatal(rawConfig)
	}
}

func TestResourceProvider_RequireTestPath(t *testing.T) {
	debug := false
	apiServerObjects := make(map[string]map[string]interface{})

	svr := fakeserver.NewFakeServer(8085, apiServerObjects, true, debug, "")
	svr.StartInBackground()

	rp := Provider()
	raw := map[string]interface{}{
		"uri":       "http://127.0.0.1:8085/",
		"test_path": "/api/objects",
	}

	rawConfig := terraform.NewResourceConfigRaw(raw)

	d := rp.Configure(context.Background(), rawConfig)
	if d.HasError() {
		t.Fatal(rawConfig)
	}

	/* Now test the inverse */
	rp = Provider()
	raw = map[string]interface{}{
		"uri":       "http://127.0.0.1:8085/",
		"test_path": "/api/apaththatdoesnotexist",
	}

	rawConfig = terraform.NewResourceConfigRaw(raw)

	d = rp.Configure(context.Background(), rawConfig)
	if !d.HasError() {
		t.Fatalf("Provider was expected to fail when visiting %v at %v but it did not!", raw["test_path"], raw["uri"])
	}

	svr.Shutdown()
}
